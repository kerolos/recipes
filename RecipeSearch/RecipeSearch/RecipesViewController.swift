//
//  ViewController.swift
//  RecipeSearch
//
//  Created by kero1 on 9/3/18.
//  Copyright © 2018 kero.dev. All rights reserved.
//

import UIKit
import RxSwift
import SDWebImage
import RxCocoa

class RecipesViewController: UIViewController {

    // should be injected
    let disposeBag = DisposeBag()
    var viewModel: RecipesViewModel? = RecipesViewModel()
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var recipesTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        recipesTableView.delegate = self
        recipesTableView.dataSource = self
        setupBindings()
//        viewModel?.query = "Hainanese Chicken Rice"
        recipesTableView.estimatedRowHeight = UITableViewAutomaticDimension
    }

    func setupBindings(){
        viewModel?.dataUpdatedObservable.subscribeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] val in
            
            self.recipesTableView.reloadData()
        }).disposed(by: disposeBag)
        
        self.searchBar.rx.searchButtonClicked.subscribe(onNext: { [unowned self] in
            if (self.searchBar.text?.isEmpty ?? true  ){
                self.viewModel?.query = nil
            }
            else{
                self.viewModel?.query = self.searchBar.text
            }
        }).disposed(by: disposeBag)
        
        self.searchBar.rx.cancelButtonClicked.subscribe(onNext: { [unowned self] in
            self.viewModel?.query = nil
        }).disposed(by: disposeBag)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension RecipesViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.dataCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RecipePreviewTableViewCell.reuseIdentifier) as! RecipePreviewTableViewCell
        
        
        let item = viewModel?.getItem(index: indexPath.row)
        
        
        cell.recipeImage?.sd_setImage(with: item?.imageUrl, completed: nil)
        cell.publisherLabel.text = item?.source
        cell.recipeTitleLabel.text = item?.title
        cell.healthLabelsStack.arrangedSubviews.forEach({ $0.removeFromSuperview()})
        cell.healthLabelsStack.subviews.forEach({ $0.removeFromSuperview()})
        
        item?.healthLabels?.forEach({ (tag) in
            let l = UILabel();
            l.text = tag
            cell.healthLabelsStack.addArrangedSubview(l)
        })
        
        
        if indexPath.row + 1 == viewModel?.dataCount{
            viewModel?.queryForNextPageIfExists()
        }
        
        return cell
    }
    
}

extension RecipesViewController: UITableViewDelegate{
    
}
