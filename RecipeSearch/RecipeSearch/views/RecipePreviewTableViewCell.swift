//
//  PecipePreviewTableViewCell.swift
//  RecipeSearch
//
//  Created by kero1 on 9/4/18.
//  Copyright © 2018 kero.dev. All rights reserved.
//

import UIKit

class RecipePreviewTableViewCell: UITableViewCell {

    public static let reuseIdentifier = "id_RecipePreviewTableViewCel"
    
    @IBOutlet weak var publisherLabel: UILabel!
    
    @IBOutlet weak var recipeTitleLabel: UILabel!
    
    @IBOutlet weak var healthLabelsStack: UIStackView!
    @IBOutlet weak var recipeImage: UIImageView!
    
    @IBOutlet weak var ratingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
