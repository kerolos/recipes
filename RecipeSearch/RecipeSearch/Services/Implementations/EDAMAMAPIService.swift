//
//  Food2ForkAPIService.swift
//  RecipeSearch
//
//  Created by kero1 on 9/3/18.
//  Copyright © 2018 kero.dev. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import RxSwift

class EDAMAMAPIService: APIService{
    
    
    private static var manager: Alamofire.SessionManager = {
        
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "*.edamam.com": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    func search(query: String, page: Int) -> Observable<EDAMAMAPISearchResponseModel?> {
        let pageSize = 30
        let from = page * pageSize
        return search(query: query, from: from, pageSize: pageSize)
        
    }
    
    // https://api.edamam.com/search?q=chicken&from=0&to=100&app_id=89193820&app_key=b0c4f8afaa13f79ee3219775e0d90d23
    let baseURL = "https://api.edamam.com"
    //search//" //?
    let id = "ec100871"
    let key = "2edb933daf51f1726c94e12dd5f4db7f" //&q=chicken /
    
    func search(query: String, from: Int, pageSize: Int) -> Observable<EDAMAMAPISearchResponseModel?> {
        let publishSubject = PublishSubject<EDAMAMAPISearchResponseModel?>()
        
        var components = URLComponents(string: baseURL + "/" + "search" )
        
//        components?.path = "search"
        components?.queryItems = [URLQueryItem(name: "app_id", value: id),
                                  URLQueryItem(name: "app_key", value: key),
                                  URLQueryItem(name: "q", value: query),
                                  URLQueryItem(name: "from", value: "\(from)"),
                                  URLQueryItem(name: "to", value: "\(from + pageSize)")]
        
        
        
        EDAMAMAPIService.manager.request((components?.url)!).responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
                let val = EDAMAMAPISearchResponseModel(JSON: json as! [String: Any])
                publishSubject.onNext( val )
                publishSubject.onCompleted()
                return
            }
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
            }
            
            
            publishSubject.onError((response.error ?? nil)!)
        }
        
        return publishSubject.asObservable()
    }
    
    
}
