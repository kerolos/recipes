//
//  APIService.swift
//  RecipeSearch
//
//  Created by kero1 on 9/3/18.
//  Copyright © 2018 kero.dev. All rights reserved.
//

import Foundation
import RxSwift
protocol APIService {
    func search( query: String, page: Int) -> Observable<EDAMAMAPISearchResponseModel?>
    
}
