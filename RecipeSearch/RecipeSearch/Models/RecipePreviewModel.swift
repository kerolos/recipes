import Foundation
import ObjectMapper

struct RecipePreviewModel : Mappable {
	var source : String?
	var url : String?
	var label : String?
	var source_url : String?
	var recipe_id : String?
	var image_url : String?
	var health_labels : [String]?
	var publisher_url : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		source <- map["recipe.source"]
		url <- map["recipe.url"]
		label <- map["recipe.label"]
		source_url <- map["source_url"]
		recipe_id <- map["recipe_id"]
		image_url <- map["recipe.image"]
		health_labels <- map["recipe.healthLabels"]
		publisher_url <- map["publisher_url"]
	}

}
