import Foundation
import ObjectMapper

struct EDAMAMAPISearchResponseModel : Mappable {
	var count : Int?
	var hits : [RecipePreviewModel]?
    var from : Int?
    var to: Int?
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		count <- map["count"]
		from <- map["from"]
        to <- map["to"]
        
        hits <- map["hits"]
        
	}

}

