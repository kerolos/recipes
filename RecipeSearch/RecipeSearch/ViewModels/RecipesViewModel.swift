//
//  RecipesViewModel.swift
//  RecipeSearch
//
//  Created by kero1 on 9/3/18.
//  Copyright © 2018 kero.dev. All rights reserved.
//

import Foundation
import RxSwift
struct RecipePreviewViewData {
    var imageUrl: URL?
    var title: String?
    var source: String?
    var healthLabels: [String]?
    
}

class RecipesViewModel{
    
    private let disposeBag = DisposeBag()
    
    let apiService: APIService

    
    let pageSize = 30
    private let maxPages = 100000
    private var lastPageReached = false
    private var data:[[RecipePreviewModel]] = []

    private let queryVariable = Variable<String?>(nil)
    public var query: String?{
        get{
            return queryVariable.value
        }
        set{
            queryVariable.value = newValue
        }
    }
    
    private let dataUpdatedPublishSubject: PublishSubject<Bool> = PublishSubject()
    
    public var dataUpdatedObservable: Observable<Bool>{
        return dataUpdatedPublishSubject.asObservable()
    }
    
    
    // should be injected //TODO: later remove defaults
    init(apiService: APIService = EDAMAMAPIService() ) {
        self.apiService = apiService
        setUpBindings()
    }
    private func setUpBindings(){
        queryVariable.asObservable().throttle(0.3, scheduler: MainScheduler.instance).subscribe(onNext: { [unowned self] (query) in
            self.lastPageReached = false
            
            self.data = []
            if (query != nil){
                self.queryForNextPageIfExists()
            }
        }).disposed(by: disposeBag)
    }
    
    var dataCount: Int{
        return data.reduce(0, { (result, arr) -> Int in
            result + arr.count
        })
    }
    
    public func getItem(index: Int) -> RecipePreviewViewData?{
        let pageIndex = index / pageSize
        guard data.count > index / pageSize
        else {
            return nil
        }
        var page = data[pageIndex]
        let item = page[index % pageSize]
        
        return getItemViewData( item )
        
    }
    
    private func getItemViewData(_ model: RecipePreviewModel) -> RecipePreviewViewData{
        return RecipePreviewViewData(imageUrl: URL(string:model.image_url ?? ""), title: model.label, source: model.source, healthLabels: model.health_labels)
    }
    
    func queryForNextPageIfExists(){
        if lastPageReached || query == nil || query == "" {
            return
        }
        if dataCount % pageSize == 0{
            let pageIndex = dataCount / pageSize
            if (pageIndex < maxPages ){
                getNextPage(query: query!, page: pageIndex + 1)
            }
        } else{
            // otherwise no data as the last page has items less than the pagelimit
        }
    }
    
    private func getNextPage(query: String, page: Int){
        apiService.search(query: query, page: page).subscribeOn (SerialDispatchQueueScheduler(internalSerialQueueName: "api_background") ) .subscribe(onNext: {[unowned self] (responseModel) in
            if (responseModel?.count)! < self.pageSize
            {
                self.lastPageReached = true
            }
            self.data.append( responseModel?.hits ?? [])
            self.dataUpdatedPublishSubject.onNext(true)
            
        }).disposed(by: disposeBag)
    }
    
    
}
